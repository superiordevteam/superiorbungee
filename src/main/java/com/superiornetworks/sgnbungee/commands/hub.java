package com.superiornetworks.sgnbungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;

public class hub extends Command
    {

    public hub()
        {
        super("hub");
        }

    @Override
    public void execute(CommandSender commandSender, String[] args)
        {
        ServerInfo hub = ProxyServer.getInstance().getServerInfo("hub");
        if (commandSender instanceof ProxiedPlayer)
            {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            player.connect(hub);
            }
        }
    }
