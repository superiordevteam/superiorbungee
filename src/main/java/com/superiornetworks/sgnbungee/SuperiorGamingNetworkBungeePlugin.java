package com.superiornetworks.sgnbungee;

import com.superiornetworks.sgnbungee.commands.hub;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.TaskScheduler;
import com.superiornetworks.sgnbungee.SGNBP_Utils;

public class SuperiorGamingNetworkBungeePlugin extends Plugin
    {

    @Override
    public void onEnable()
        {
        getProxy().getPluginManager().registerListener(this, new SuperiorGamingNetworkBungeePluginListener());
        getProxy().getPluginManager().registerCommand(this, new hub());
        Runnable host = new Runnable()
            {
            @Override
            public void run()
                {
                    getProxy().broadcast(SGNBP_Utils.colorize("&8&l[&2&lSuperior Gaming Network Notice&8&l]  &4This Network is hosted by Superior-Networks: http://www.superior-networks.com &3~ &2Quality Hosting at cost effective prices"));
                }
            };
        Runnable forums = new Runnable()
            {
            @Override
            public void run()
                {
                   // getProxy().broadcast(new ComponentBuilder("Visit our forums at http://gaming.superior-networks.com/foruns for info on the server, its staff and much more!").color(ChatColor.BLUE).create());
                    getProxy().broadcast(SGNBP_Utils.colorize("&8&l[&2&lSuperior Gaming Network Notice&8&l] &dVisit our forums at &1http://gaming.superior-networks.com/foruns &dfor all the information on this network!"));
                }
            };
        
        Runnable managers = new Runnable()
            {
            @Override
            public void run()
                {
                   // getProxy().broadcast(new ComponentBuilder("Visit our forums at http://gaming.superior-networks.com/foruns for info on the server, its staff and much more!").color(ChatColor.BLUE).create());
                    getProxy().broadcast(SGNBP_Utils.colorize("&8&l[&2&lSuperior Gaming Network Notice&8&l] &4Our Network is run by &5Wild1145, &9Camzie99 &2and &aKyled1986"));
                }
            };
        
        
        
        getProxy().getScheduler().schedule(this, forums, 160, 300, TimeUnit.SECONDS);
        getProxy().getScheduler().schedule(this, host, 10, 300, TimeUnit.SECONDS);
        getProxy().getScheduler().schedule(this, managers, 60, 300, TimeUnit.SECONDS);
        }
    }
