package com.superiornetworks.sgnbungee;

import net.md_5.bungee.api.ChatColor;

public class SGNBP_Utils {
    
    public static String colorize(String string)
    {
        return ChatColor.translateAlternateColorCodes('&', string);
    }
    
}
