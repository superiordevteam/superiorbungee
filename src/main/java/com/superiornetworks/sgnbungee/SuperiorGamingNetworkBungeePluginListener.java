package com.superiornetworks.sgnbungee;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class SuperiorGamingNetworkBungeePluginListener implements Listener
    {
    @EventHandler
    public void onServerConnected(final ServerConnectedEvent event)
        {
        String name = event.getServer().getInfo().getName();
        String message;
        switch (name)
            {
            case "hub":
                message = "Welcome to the Superior Gaming Network's First Hub Server!";
                break;
            case "hub2":
                message = "Welcome to the Superior Gaming Network's Second Hub Server!";
                break;
            case "SkyBlock1":
                message = "Welcome to Superior SkyBlock; be sure to read the forum posts for instructions!";
                break;
            case "FactionsPVP1":
                message = "Welcome to Superior Factions; team up with a group of people and climb to the top of power!";
                break;
            case "Survival1":
                message = "Welcome to Superior Survival; want to play survival? Don't want other players messing with your stuff? This server is the place for you!";
                break;
            case "Murder1":
                message = "Welcome to Superior Murder; a unique game type, read the forum posts for more details.";
                break;
            case "Murder 2":
                message = "Welcome to Superior Murder; a unique game type, read the forum posts for more details.";
                break;
            case "Parkour1":
                message = "Welcome to Superior Parkour 1; so you've come for a challenge have you? Nice to know, have fun and good luck!";
                break;
            case "Parkour2":
                message = "Welcome to Superior Parkour 2; and even more difficult parkour challenge! Have fun and good luck.";
                break;
            case "Plots1":
                message = "Welcome to Creative Plots; have fun, take your time and let all worries drain away into nothingness as you freely build you days away.";
                break;
            case "AllOp1":
                message = "Welcome to Superior Freedom; here you automatically get free op and can mess around with a variety of plugins, but be careful and follow the rules!";
                break;
            default:
                message = "Welcome to " + name + "!";
                break;
            }
        event.getPlayer().sendMessage(new ComponentBuilder(message).color(ChatColor.BLUE).create());
        }

    @EventHandler
    public void onPlayerKicked(ServerKickEvent event)
        {
        event.setCancelServer(ProxyServer.getInstance().getServerInfo("hub"));
        event.getPlayer().sendMessage(new ComponentBuilder("You have been returned to the hub, If you feel that this is an error please contact a system adminstrator" + " " + "message:" + event.getKickReason()).color(ChatColor.RED).create());
        event.setCancelled(true);
        }
    }
